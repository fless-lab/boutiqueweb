/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;


import com.flessapps.boutique.entites.Entite;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fless
 * @param <T>
 * 
 */
public abstract class Service<T extends Entite> {
    
    private final List<T> liste;
    
    public Service(){
        liste = new ArrayList<>();
    }
    
    public Service(List<T> liste){
        this.liste=liste;
    }
    
    public void ajouter(T t){
        liste.add(t);
    }
    
    public void modifier(T t){
        for(T myT : liste) {
            if(myT.getId()== t.getId()){
                liste.set(liste.indexOf(myT), t);
            }
            return;
        }
    }
    
    
    public T trouver(Integer id){
        for(T t : liste){
            if(t.getId() == id){
                return t;
            }
        }
        return null;
    }
    
    public void supprimer(Integer id){
        T elt = liste.get(id);
        liste.remove(elt);
    }
    
    public void supprimer(T e){
        liste.remove(e);
    }
    
    public List<T> lister(){
        return liste;
    }
    
    public List<T> lister(int debut, int nombre){
        List<T> maListe = new ArrayList<>();
        for(int i=debut;i<nombre+debut-1;++i){
            maListe.add(liste.get(i));
        }
        return maListe;
    }
}


