/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.ProduitAchete;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.ProduitAcheteService;

/**
 *
 * @author fless
 */
@Path("/produits_achetes")
public class ProduitAcheteResource {
    
    ProduitAcheteService produitAcheteService = new ProduitAcheteService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProduitAchete> listerProduitAchete(){
        return produitAcheteService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ProduitAchete> listerProduitAchete(@QueryParam("start") int debut,@QueryParam("n") int nombre){
         return produitAcheteService.lister(debut, nombre);
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{produitAcheteId}")
    public ProduitAchete trouverProduitAchete(@PathParam("produitAcheteId") Integer produitAcheteId){
        return produitAcheteService.trouver(produitAcheteId);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterProduitAchete(ProduitAchete produitAchete){
        produitAcheteService.ajouter(produitAchete);
    }
    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierProduitAchete(ProduitAchete produitAchete){
        produitAcheteService.modifier(produitAchete);
    }
    
    @DELETE
    @Path("/{produitAcheteId}")
    public void supprimerProduitAchete(@PathParam("produitAcheteId")Integer produitAcheteId){
        produitAcheteService.supprimer(produitAcheteId);  
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerProduitAchete(ProduitAchete produitAchete){
        produitAcheteService.supprimer(produitAchete);
    }
}
