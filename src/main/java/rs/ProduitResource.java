/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Produit;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.ProduitService;

/**
 *
 * @author fless
 */
@Path("/produits")
public class ProduitResource {
    
    ProduitService produitService = new ProduitService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produit> listerProduit(){
        return produitService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Produit> listerProduit(@QueryParam("start") int debut,@QueryParam("n") int nombre){
         return produitService.lister(debut, nombre);
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{produitId}")
    public Produit trouverProduit(@PathParam("produitId") Integer produitId){
        return produitService.trouver(produitId);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterProduit(Produit produit){
        produitService.ajouter(produit);
    }
    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierProduit(Produit produit){
        produitService.modifier(produit);
    }
    
    @DELETE
    @Path("/{produitId}")
    public void supprimerProduit(@PathParam("produitId")Integer produitId){
        produitService.supprimer(produitId);  
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerProduit(Produit produit){
        produitService.supprimer(produit);
    }
}
