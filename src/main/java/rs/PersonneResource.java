/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Personne;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.PersonneService;

/**
 *
 * @author fless
 */
@Path("/personne")
public class PersonneResource {
    
    PersonneService personneService = new PersonneService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Personne> listerPersonne(){
        return personneService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Personne> listerPersonne(@QueryParam("start") int debut,@QueryParam("n") int nombre){
         return personneService.lister(debut, nombre);
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{personneId}")
    public Personne trouverPersonne(@PathParam("personneId") Integer personneId){
        return personneService.trouver(personneId);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterPersonne(Personne personne){
        personneService.ajouter(personne);
    }
    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierPersonne(Personne personne){
        personneService.modifier(personne);
    }
    
    @DELETE
    @Path("/{personneId}")
    public void supprimerPersonne(@PathParam("personneId")Integer personneId){
        personneService.supprimer(personneId);  
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerPersonne(Personne personne){
        personneService.supprimer(personne);
    }
}
