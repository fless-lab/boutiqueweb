/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Categorie;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.CategorieService;

/**
 *
 * @author fless
 */
@Path("/categories")
public class CategorieResource {
    
    CategorieService categorieService = new CategorieService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categorie> listerCategorie(){
        return categorieService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Categorie> listerCategorie(@QueryParam("start") int debut,@QueryParam("n") int nombre){
         return categorieService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{categorieId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Categorie trouverCategorie(@PathParam("categorieId") Integer categorieId){
        return categorieService.trouver(categorieId);
    }

    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterCategorie(Categorie categorie){
        categorieService.ajouter(categorie);
    }
    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierCategorie(Categorie categorie){
        categorieService.modifier(categorie);
    }
    
    @DELETE
    @Path("/{categorieId}")
    public void supprimerCategorie(@PathParam("categorieId")Integer categorieId){
        categorieService.supprimer(categorieId);  
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerCategorie(Categorie categorie){
        categorieService.supprimer(categorie);
    }
    
    
}
