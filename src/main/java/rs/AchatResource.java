/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.flessapps.boutique.entites.Achat;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.AchatService;

/**
 *
 * @author fless
 */
@Path("/achats")
public class AchatResource {
    
    AchatService achatService = new AchatService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Achat> listerAchat(){
        return achatService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Achat> listerAchat(@QueryParam("start") int debut,@QueryParam("n") int nombre){
         return achatService.lister(debut, nombre);
    }
    
    
    @GET
    @Path("/{achatId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Achat trouverAchat(@PathParam("achatId") Integer achatId){
        return achatService.trouver(achatId);
    }
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterAchat(Achat monAchat){
        achatService.ajouter(monAchat);
    }
    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierAchat(Achat achat){
        achatService.modifier(achat);
    }
    
    @DELETE
    @Path("/{achatId}")
    public void supprimerAchat(@PathParam("achatId")Integer achatId){
        achatService.supprimer(achatId);  
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerAchat(Achat achat){
        achatService.supprimer(achat);
    }
    

    
}