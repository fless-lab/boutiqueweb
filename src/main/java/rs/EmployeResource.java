/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;


import com.flessapps.boutique.entites.Employe;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import service.EmployeService;

/**
 *
 * @author fless
 */
@Path("/employes")
public class EmployeResource {
    
    EmployeService employeService = new EmployeService();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employe> listerEmploye(){
        return employeService.lister();
    }
    
    @GET
    @Path("/filtrer")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employe> listerEmploye(@QueryParam("start") int debut,@QueryParam("n") int nombre){
         return employeService.lister(debut, nombre);
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{employeId}")
    public Employe trouverEmploye(@PathParam("employeId") Integer employeId){
        return employeService.trouver(employeId);
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void ajouterEmploye(Employe employe){
        employeService.ajouter(employe);
    }
    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void modifierEmploye(Employe employe){
        employeService.modifier(employe);
    }
    
    @DELETE
    @Path("/{employeId}")
    public void supprimerEmploye(@PathParam("employeId")Integer employeId){
        employeService.supprimer(employeId);  
    }
    
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    public void supprimerEmploye(Employe employe){
        employeService.supprimer(employe);
    }
    
}
