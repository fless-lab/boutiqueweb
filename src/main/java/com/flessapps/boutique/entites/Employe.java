/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flessapps.boutique.entites;

/**
 *
 * @author fless
 */
import java.time.LocalDate;
import java.util.Objects;


public class Employe extends Personne{
    
    private String cnss;
    private LocalDate dateEmbauche;

    
    public Employe(){}
    
    public Employe(String cnss, LocalDate dateEmbauche, int id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
    }

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    @Override
    public String toString() {
        return "Employe{" +
                "cnss='" + cnss + '\'' +
                ", dateEmbauche=" + dateEmbauche +
                ", id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employe other = (Employe) obj;
        if (!Objects.equals(this.cnss, other.cnss)) {
            return false;
        }
        return Objects.equals(this.dateEmbauche, other.dateEmbauche);
    }

  
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.cnss);
        hash = 89 * hash + Objects.hashCode(this.dateEmbauche);
        return hash;
    }
    
    

    
}
