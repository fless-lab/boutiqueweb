/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flessapps.boutique.entites;
import java.io.Serializable;

/**
 *
 * @author fless
 */
public class ProduitAchete extends Entite{
    
    private int quantite;
    private double remise;
    private Produit produit;

    
    public ProduitAchete(){}
    
    public ProduitAchete(long id, Produit produit,int quantite, double remise) {
        this.id = id;
        this.produit=produit;
        this.quantite = quantite;
        this.remise = remise;
    }


    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    
    
    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }
    
    public double getPrixTotal(){
        return quantite*produit.getPrixUnitaire() - remise*quantite*produit.getPrixUnitaire();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.quantite;
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        if (this.quantite != other.quantite) {
            return false;
        }
        return Double.doubleToLongBits(this.remise) == Double.doubleToLongBits(other.remise);
    }

    @Override
    public String toString() {
        return "ProduitAchete{ Produit : " + produit.getLibelle() + ", quantite=" + quantite + ", remise=" + remise + '}';
    }

    
    
}
