/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.flessapps.boutique.entites;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;
import java.util.List;
/**
 *
 * @author fless
 */
public class Achat extends Entite{
    

    private double remise;
    private LocalDate dateAchat;
    private List<ProduitAchete> produitsAchetes = new ArrayList();
    
    
    public Achat(){}
    
    public Achat(long id, double remise, LocalDate dateAchat,ArrayList<ProduitAchete> produitsAchetes) {
        this.id = id;
        this.remise = remise;
        this.dateAchat = dateAchat;
        this.produitsAchetes = produitsAchetes;
    }

    
    

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }
    
    
    public List<ProduitAchete> getProduitsAchetes(){
        return this.produitsAchetes;
    }
    
    public void setProduitsAchetes(ProduitAchete produitsAchetes){
        this.produitsAchetes.add(produitsAchetes);
    }
    
    public void effectuerAchat(ProduitAchete produit){
        this.produitsAchetes.add(produit);
    }
    
    public void annulerAchat(ProduitAchete produit){
        this.produitsAchetes.remove(produit);
    }
    
    
    //La remise totale correspond a la somme des remise de chaque produit acheté et la remise sur l'achat en question
    public double getRemiseTotale(){
        double remiseT=0;
        for(ProduitAchete pa : produitsAchetes){
            remiseT+=pa.getRemise();
        }
        return remiseT+remise;
    }
    
    public double getPrixTotal(){
        double prix=0;
        for(ProduitAchete pa : produitsAchetes){
            prix+=pa.getPrixTotal();
        }
        return prix*(1-remise);
    }
   

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 89 * hash + Objects.hashCode(this.dateAchat);
        hash = 89 * hash + Objects.hashCode(this.produitsAchetes);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", remise=" + remise + ", dateAchat=" + dateAchat + ", produitsAchetes=" + produitsAchetes.toString() + '}';
    }
   
}
