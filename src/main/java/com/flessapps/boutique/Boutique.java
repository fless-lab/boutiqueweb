package com.flessapps.boutique;

import com.flessapps.boutique.entites.Achat;
import com.flessapps.boutique.entites.Categorie;
import com.flessapps.boutique.entites.Client;
import com.flessapps.boutique.entites.Personne;
import com.flessapps.boutique.entites.Produit;
import com.flessapps.boutique.entites.ProduitAchete;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author fless
 */
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class Boutique {

    public static void main(String[] args) {
        //creation et afficharge d'une personne
        Personne moi = new Client("ECOBANK", "223310", 003, "ABA", "jeff", LocalDate.parse("2011-05-11"));
        System.out.println(moi.toString());
        System.out.println(" Le client " + moi.getNom() + " " + moi.getPrenom() + " a " + moi.getAge() + " ans");

        System.out.println("\n##################################################################\n");
        
        //creation des categories de produit
        Categorie fruits = new Categorie(001, "Fruits", "Ici sont repertorié les fruits.");
        Categorie outilInfo = new Categorie(002, "Informatique", "Ici sont repertorié les outils liés à l'informatique.");
        Categorie btp = new Categorie(003, "Batiments", "Ici sont repertorié tout ce qui concerne les batiment et leurs outils.");
        Categorie divers = new Categorie(004, "Autres", "On y trouve un peu de tout.");

        
        //test d'un produit (périmé ou pas)
        Produit prod = new Produit(302321, "BodyBuilder Maker", 34.5, LocalDate.parse("2020-12-03"), divers);
        System.out.println(prod.toString());
        if (prod.estPerime()) {
            System.out.println("Produit(Catégorie:" + divers.getLibelle() + ") : " + prod.getLibelle() + " est périmé depuis le " + prod.getDatePeremption());
        } else {
            System.out.println("Produit (Catégorie:" + divers.getLibelle() + ") est en bonne forme et expirera le " + prod.getDatePeremption());
        }
        
        System.out.println("\n##################################################################\n");

        //creation des produit
        Produit pA = new Produit(111, "Ananas", 3.5, LocalDate.parse("2021-02-19"), fruits);
        Produit pB = new Produit(112, "Orange", 9, LocalDate.parse("2020-02-19"), fruits);
        Produit pC = new Produit(113, "Tournevis", 17.5, LocalDate.parse("2050-12-31"), btp);
        Produit pD = new Produit(114, "Mangue", 12, LocalDate.parse("2021-05-11"), fruits);
        Produit pE = new Produit(115, "Laptop Dell", 4, LocalDate.parse("2029-01-03"), outilInfo);
        Produit pF = new Produit(116, "Desinfectant", 6.5, LocalDate.parse("2021-12-19"), divers);
        Produit pG = new Produit(117, "Disc Dur", 8.5, LocalDate.parse("2100-09-23"), outilInfo);

        
        //test d'egalite
        if (pA.equals(pG)) {
            System.out.println("Les deux articles pA et pH sont égaux");
        } else {
            System.out.println("Les articles pA et pH sont differents");
        }

        System.out.println("\n##################################################################\n");
        
        //produit acheté
        ProduitAchete paA = new ProduitAchete(1,pA, 3, 0.2);
        ProduitAchete paB = new ProduitAchete(2,pB, 4, 0.1);
        ProduitAchete paC = new ProduitAchete(3,pC, 9, 0.0);
        ProduitAchete paD = new ProduitAchete(4,pD, 1, 0.1);

        //List des produit acheté
        ArrayList<ProduitAchete> courses = new ArrayList<ProduitAchete>();
        courses.add(paA);
        courses.add(paB);
        courses.add(paC);
        courses.add(paD);
        System.out.println(courses.toString());
        
        //test pour avoir le prix total de chaque produit acheté (lors de cette course ) 
        //en fonction de la quantite et de la remise
        for (ProduitAchete p : courses){
            System.out.println(p+" : "+p.getPrixTotal());
        }

        
        //Creation d'achat et calculons la remise totale et  le prix total de cette course
        Achat achat = new Achat(103321,0.15,LocalDate.now(),courses);
        System.out.println(achat.toString());
        System.out.println(achat.getRemiseTotale());
        System.out.println(achat.getPrixTotal());
    }

}
